# Created by Theophanis Tsandilas

#######################################
summary.table.wit <- function(filename){
    data <- read.csv(file=filename, header=TRUE, sep=",")
    
    data <- data[,c(2,3,4,5,6)]
    data$time <- data$time/ 1000000
    data$totalTime <- data$totalTime/ 1000000
    
    data
}

aggregate.wit <- function(df){
    df$dist <- (1/df$dist)^5 # This seems to work well!!!
    
    df.1 <- aggregate(dist~queryID, df, FUN = sum)
    df$product <- df$dist*df$distance
    df <- aggregate(product~queryID, df, FUN = sum)
    
    df$distance <- df$product/df.1$dist
    
    df[-c(2)]
}

createInitialEstimate <- function(wit.file, queries.file, dist.file, qid){
    # Data preparation #######################################
    queries <- summary.table.wit(queries.file)
    queries$distance <- sqrt(queries$distance)
    queries <- queries[, c(1, 3)]

    witnesses <- read.csv(file=wit.file, header=TRUE, sep=",")
    witnesses <- witnesses[,c(2,4)]
    witnesses$distance <- sqrt(witnesses$distance)
    quer.wit <- read.csv(file=dist.file, header=TRUE, sep=",")

    colnames(queries)[1] <- "queryID"
    colnames(witnesses)[1] <- "witnessID"
    colnames(quer.wit)[3] <- "dist"
    data <- merge(quer.wit, witnesses, by = "witnessID")
    
    df <- aggregate.wit(data)
    training <- queries[-c(1:200),]
    training <- training[sample(nrow(training), 100),]
    
    df.1 <- merge(df, training, by="queryID")
    lmodel <- lm(distance.y ~ distance.x, data = df.1)
    
    query <- queries[queries$queryID == qid,]
    df.2 <- merge(df, query, by="queryID")
    pi.wit(df.2$distance.x, lmodel)
}


pi.wit <- function(wit, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = wit)
    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
    #In addition to the CI, also report the meab of the normal distirbution and its SD
    alpha <- 1 - level
    m <- pi[1]
    sd <- (pi[3] - pi[2])/(2*qnorm(1 - alpha/2))
    
    c(max(0, pi[1]), max(0, pi[2]), pi[3], m, sd)
}

##### Time estimate
require("quantreg")

quantile.summary.table <- function(filename){
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    
    data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    data.bsf$time <- log2(data.bsf$time)
    data.bsf$totalTime <- log2(data.bsf$totalTime)
    
    data.bsf$leavesVisited <- log2(data.bsf$leavesVisited)
    data.bsf$totalLeavesVisited <- log2(data.bsf$totalLeavesVisited)
    
    #data.bsf$leavesVisited <- log2(data.bsf$time)
    #data.bsf$totalLeavesVisited <- log2(data.bsf$totalTime)
    
    data.bsf$distance <- sqrt(data.bsf$distance )
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    
    merge(data.bsf, best, by="queryId")[,c(1,2,4,5,6,7,3,9)]
}

getTrainingFrame <- function(data){
    appr <- aggregate(distance.x ~ queryId, data = data, max)
    best <- data[data$answerId.x=="bestNN",]
    merge(appr, best, by="queryId")
}

pi <- function(dist, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = dist)
    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
    c(pi[1], max(0, pi[2]), min(dist, pi[3]))
}


estimateTimeBound <- function(trainingfile, qid, phi = .05, N = 200){
    queries <- quantile.summary.table(trainingfile)
    query <- queries[queries$queryId == qid,]
    
    queries <- queries[queries$queryId>=200,]
    training.ids <- sample(200:999, N)
    training <- queries[queries$queryId %in% training.ids,]
    
    training <- getTrainingFrame(training)[c(2,4,6)]
    names(training) <- c("distance", "time", "leavesVisited")
    
    print(training[1:10,])
    
    lmodel <- rq(leavesVisited ~ distance, data = training, tau = 1 - phi)
    slope <- lmodel$coefficients[[2]]
    intercept <- lmodel$coefficients[[1]]
    predict.leaves = query[1,]$distance.x * slope + intercept

    tmodel <- rq(time ~ distance, data = training, tau = 1 - phi)
    slope <- tmodel$coefficients[[2]]
    intercept <- tmodel$coefficients[[1]]
    predict.time = query[1,]$distance.x * slope + intercept
    
    c(2^predict.leaves, 2^predict.time)
}
