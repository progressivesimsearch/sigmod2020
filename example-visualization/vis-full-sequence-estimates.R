# Created by Theophanis Tsandilas

rm(list=ls())

library("TSdist")
library("jmotif")
library(yarrr)

source("models/initial-model.R")
source("models/progressive-model.R")

######################################################
getSeries <- function(dataset, id){
    series <- dataset[dataset$queryID == id,][-c(1)]
    ts(c(series))
}

getAnswers <- function(dataset, id){
    answers <- dataset[dataset$queryID == id,][-c(1)]
}

########################################################
showInitialPlot <- function(pi, bounds, ymax = 16){
    par(mar=c(.4, 3, 3, 0))
    
    # Generate a random normal distribution with the given characteristics and bound it by the current distance
    distr <- rnorm(1000, mean = pi[4], sd = pi[5])
    distr <- sapply(distr, function(x){max(x, 0)})
    
    estimate <- data.frame(distance = distr)
    
    pirateplot(data = estimate, theme = 0, #ylab = "distance",
        pal = "skyblue2", # southpark color palette
        bean.f.o = 1, # Bean fill
        point.o = 0, # Points
        avg.line.o = 0, # Average line
        bar.f.o = 0, # Bar
        inf.f.col = "white", # Inf fill col
        inf.b.col = "black", # Inf border col
        avg.line.col = "black", # avg line col
        #bar.f.col = gray(1), # bar filling color
        point.bg = "white",
        point.col = "black",
        gl.lwd = 0,
        #      avg.line.fun = fix,
        quant = c(.025, .975), quant.lwd = 1.5, quant.col = "red",
        xlab = NA, xaxt ="n", yaxt = "s", ylim = c(0, ymax), bty="n"
    )
    text(1, pi[3] + 2, "distance", xpd=NA)
    
    text(0.2, ymax + 4, paste("1NN in < ", round(bounds[2], digits=1), " sec (", ceiling(bounds[1]), " leaves)", sep=""), cex=1.3, xpd=NA, adj = 0)
    
    #  axis(2, at=c(10, 14, 18, 22))
    #axis(side = 2, at = c(0, 4, 8, 12, 16), labels = ylabels)
    
}


percent <- function(x, digits = 0, format = "f", ...) {
    paste0(formatC(100 * x, format = format, digits = digits, ...), "%")
}

########################################################
showProgressivePlot <- function(d1nn, pi, probability, query, ymax = 16){
    
    # Generate a random normal distribution with the given characteristics and bound it by the current distance
    distr <- rnorm(1000, mean = pi[4], sd = pi[5])
    distr <- sapply(distr, function(x){min(x, query$distance)})
    
    estimate <- data.frame(distance = distr)
    
    fix <- function(x){
        query$distance
    }
    
    par(mar=c(.4, 3, 3, 0))
    #    par(mar=c(.4, 3, 1, 0))
    pirateplot(data = estimate, theme = 0, #ylab = "distance",
        pal = "skyblue2", # southpark color palette
        bean.f.o = 1, # Bean fill
        point.o = 0, # Points
        avg.line.o = 1, # Average line
        bar.f.o = 1, # Bar
        inf.f.col = "white", # Inf fill col
        inf.b.col = "black", # Inf border col
        avg.line.col = "black", # avg line col
        avg.line.lwd = 3,
        bar.f.col = "gray95", # bar filling color
        point.bg = "white",
        point.col = "black",
        avg.line.fun = fix,
        gl.lwd = 0,
        quant = c(.025, .975), quant.lwd = 1.5, quant.col = "red",
        xlab = NA, xaxt ="n", yaxt = "s", ylim = c(0, ymax), bty="n"
    )
    text(1, fix(0) + 2, "distance", xpd=NA)
    
    text(0.2, ymax + 4, paste("1NN probability = ", percent(probability), sep=""), cex=1.3, xpd=NA, adj = 0)
    
    #axis(2, at=c(10, 14, 18, 22))
    #axis(side = 2, at = c(0, 4, 8, 12, 16), labels = ylabels)
    
    distr.error <- 100*((query$distance / distr) - 1)
    estimate.error <- data.frame(error = distr.error)
    
    fix_error <- function(x){100*((query$distance / d1nn) - 1)}
    
    par(mar=c(.4, 2, 3, 0.5))
    pirateplot(data = estimate.error, theme = 0, #ylab = "distance",
        pal = "goldenrod1", # southpark color palette
        bean.f.o = 1, # Bean fill
        point.o = 0, # Points
        avg.line.o = 1, # Average line
        bar.f.o = 0, # Bar
        inf.f.col = "white", # Inf fill col
        inf.b.col = "black", # Inf border col
        avg.line.col = "green3", # avg line col
        #bar.f.col = gray(1), # bar filling color
        point.bg = "white",
        point.col = "black",
        avg.line.fun = fix_error,
        avg.line.lwd = 1,
        gl.lwd = 0,
        quant = c(.025, .975), quant.lwd = 1.5, quant.col = "red",
        xlab = NA, xaxt ="n", yaxt = "s", bty="n", ylim = c(0,40)
    )
    text(1, quantile(distr.error, probs = c(.975)) + 4, "error (%)")
    
}

queryfile <- "data/raw_query.csv"
answerfile <- "data/raw_series.csv"
distancefile <- "data/bsf-100.csv"

trainingfile <- "data/bsf-all.csv"
queries.file <- "data/best-all.csv"
wit.file <- "data/witnesses-100-200.csv"
dist.file <- "data/dist-wit-query.csv"

ts <- c(0, 10, 12, 14)
T <- length(ts)

multiplierTime <- 20/1000000 # I multiply by this to bring time to correct scale

#qids <- c(2, 23, 53, 54, 63)
qid <- 14

##################################################################
# Preparation of the witnesss-based estimation
pi.init <- createInitialEstimate(wit.file, queries.file, dist.file, qid)
#################################################

bounds <- estimateTimeBound(trainingfile, qid)
bounds[2] <- bounds[2]*multiplierTime

models <- createProgressiveModels(trainingfile, ts)
logisticModels <- createProgressiveLogisticModels(trainingfile, ts)

data <- read.csv(file=queryfile, header=TRUE, sep=",")
data <- data[data$queryID < 100,]

data_answers <- read.csv(file=answerfile, header=TRUE, sep=",")
data_answers <- data_answers[data_answers$queryID < 100,]

distances <- read.csv(file=distancefile, header=TRUE, sep=",")[-c(1)]
distances <- distances[distances$queryId < 100,]

#par(mfrow=c(1, T + 2), mar = c(.1,1,.5,0.1))

par(mar=c(0, 1, 1.4, 0.4))
par(mgp=c(10,0.5,0))

layout(matrix(c(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
                    7:18), nrow=2, byrow = TRUE), heights=c(1,2))

query <- getSeries(data, qid)
answers <- getAnswers(data_answers, qid)
query_info <- distances[distances$queryId == qid,]
query_info$leavesVisited <- log2(query_info$leavesVisited)
query_info$totalLeavesVisited <- log2(query_info$totalLeavesVisited)
query_info$distance <- sqrt(query_info$distance)


################################### Plot the query first
plot(query, col="orangered2", lwd = 1, ylab = NA, xlab =NA, xaxt ="n", yaxt ="n", bty="n")#, main = paste("id = ", qid, sep=""))

##############################
j <- 1
c <- 1
d1nn <- query_info[nrow(query_info),]$distance
for(v in ts){
    if(is.na(query_info[j,]$totalLeavesVisited) | query_info[j,]$totalLeavesVisited < v) {
        plot(0, type='n', axes=FALSE, ann=FALSE)
        
    } else {
        if(query_info[j,]$leavesVisited < v){
            while(j < nrow(query_info) && query_info[j+1,]$leavesVisited < v) j <- j + 1
        }
        
        answer <- answers[j,][-c(1)]
        series <- ts(c(answer))
        PI <- pi(v, query_info[j,]$distance, models[[c]], level = 0.9)
        time_ = query_info[j,]$time*multiplierTime
        
        if(v == 0) title <- paste(round(time_*1000), " msec (", 2^v, " leaf)", sep="")
        else title <- paste(round(time_,digits=1), " sec (", 2^v, " leaves)", sep="")
        
        plot(series, col="dodgerblue2", lwd = 1, ylab = NA, xlab =NA, xaxt ="n", yaxt ="n", bty="n", main = title)
    }
    
    c <- c + 1
}

j <- nrow(query_info)
time_ = query_info[j,]$totalTime*multiplierTime
plot(series, col="dodgerblue2", lwd = 1, ylab = NA, xlab =NA, xaxt ="n", yaxt ="n",bty="n",
                main = paste(round(time_,digits=1), " sec (", round(2^query_info[j,]$totalLeavesVisited), " leaves)", sep="")
)

############################


showInitialPlot(pi.init, bounds)
plot(0, type='n', axes=FALSE, ann=FALSE)

j <- 1
c <- 1
for(v in ts){
    if(is.na(query_info[j,]$totalLeavesVisited) | query_info[j,]$totalLeavesVisited < v) {
        plot(0, type='n', axes=FALSE, ann=FALSE)
        plot(0, type='n', axes=FALSE, ann=FALSE)
    } else {
        if(query_info[j,]$leavesVisited < v){
            while(j < nrow(query_info) && query_info[j+1,]$leavesVisited < v) j <- j + 1
        }
        
        answer <- answers[j,][-c(1)]
        series <- ts(c(answer))
        PI <- pi(v, query_info[j,]$distance, models[[c]], level = 0.9)
        
        predicted <- lpredict(v, query_info[j,]$distance, query_info[j,]$time, logisticModels[[c]])
        
        showProgressivePlot(d1nn, PI, predicted$fit, query_info[j,])
    }
    c <- c + 1
}
#plot(0, type='n', axes=FALSE, ann=FALSE)
showProgressivePlot(d1nn, c(d1nn, d1nn, d1nn, d1nn, 0), 1, query_info[nrow(query_info),])
