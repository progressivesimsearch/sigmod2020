These have been generated with iSAX2+, but the information that we use is NOT index-depended.

For each dataset, we have a base o 1000 witnesses and 1000 queries (both for training and testing).

The *-dist.csv files have the distances between queries and witnesses.