# Created by Theophanis Tsandilas
# Plots the distribution of visited leaves for the four datasets

# Clean the memory from all objects
rm(list=ls())

#library("ggplot2")
#library(scales)

library(yarrr)

getPlot <- function(alg, dataset, ylabels = FALSE){
    file <- paste(alg, "/", dataset, ".csv", sep ="")
    
    data.bsf.1 <- read.csv(file, header=TRUE, sep=",")
    data.bsf <- data.bsf.1[,c(2,3,4,5,6,9,10)]
    data.bsf <- data.bsf[data.bsf$queryId < 100,]
    
    data.bsf$percent <- data.bsf$time / data.bsf$totalTime
    data.bsf$time <- data.bsf$time/ 1000000
    data.bsf$totalTime <- data.bsf$totalTime/ 1000000
    data.bsf$distance <- sqrt(data.bsf$distance)
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    cat("NN time = ", mean(best$time), ", min = ", min(best$time), ", max = ", max(best$time), "\n", sep ="")
    cat("Total time = ", mean(best$totalTime), ", min = ", min(best$totalTime), ", max = ", max(best$totalTime), "\n", sep ="")
    
    best2 <- best
    best2$answerId <- "end"
    best2$leavesVisited <- best2$totalLeavesVisited
    
    data <- rbind(best2, best)
    
    #cbPalette <- c("#E69F00", "#009E73", "#0072B2", "#D55E00", "#CC79A7")
    
    data$leavesVisited <- log2(data$leavesVisited)
    
    pirateplot(formula = leavesVisited ~ factor(answerId), data = data, theme = 0,
        pal = c("skyblue2", "gold2"), # southpark color palette
        bean.f.o = 1, # Bean fill
        point.o = 1, # Points
        #inf.f.o = .7, # Inference fill
        #inf.b.o = .8, # Inference border
        avg.line.o = 1, # Average line
        bar.f.o = 1, # Bar
        inf.f.col = "white", # Inf fill col
        inf.b.col = "black", # Inf border col
        avg.line.col = c("grey10","grey10"), # avg line col
        bar.f.col = "gray92", # bar filling color
        point.pch = 21,
        point.bg = "white",
        point.col = "grey30",
        point.cex = .5,
        point.lwd = .6,
        gl.lwd = 0,
        avg.line.fun = median,
        xlab = NA, xaxt ="n", yaxt = "n", ylim = c(0,17), bty="n"
    )
    axis(side = 2, at = c(0, 4, 8, 12, 16), labels = ylabels)
    #legend("topleft", horiz=TRUE,  fill = "southpark", legend = c("1NN found", "end of search"))
}

datasets <- c("synthetic", "seismic", "sald", "deep1b")
algorithms <- c("isax", "dstree")

par(mfrow=c(2, 4), mar = c(.1, 1, .1, 0.2))
getPlot(algorithms[1], datasets[1], ylabels = TRUE)
getPlot(algorithms[1] ,datasets[2])
getPlot(algorithms[1], datasets[3])
getPlot(algorithms[1], datasets[4])

getPlot(algorithms[2], datasets[1], ylabels = TRUE)
getPlot(algorithms[2] ,datasets[2])
getPlot(algorithms[2], datasets[3])
getPlot(algorithms[2], datasets[4])
