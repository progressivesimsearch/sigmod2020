The project contains the R code that we used to build and evaluate our models descibed in our SIGMOD paper. You can check the project page for more details:
http://helios.mi.parisdescartes.fr/~themisp/progrss/

The project contains the following folders (citations and figure numbers refer to the ones [in the paper](https://hal.archives-ouvertes.fr/hal-02560760/document)):
- *ciaccia-et-al.* It presents the code for the analysis of the results of the original methods of Ciacca et al. [20,21]. 
The data folder contains our raw evaluation results (%coverage) produced for different datasets, dataset sizes, and parameters.   
- *datasets.* It presents the logs that we use to train and test our estimation models. These logs provide detailed information (answer identifier, time, number of visited leaves, current distance, etc.)
about the execution of 1NN similarity search queries with two indices: DSTree and iSAX2+
- *example-motivation.* Code used to generate Figure 1, which shows the evolution of the approximate 1-NN distance for representative queries (with iSAX2+).
- *example-visualization.* Code used to generate Figure 6, which shows a visualization example that communicates progressive estimates to the user.
- *models-experiments-results.* The code in this folder evaluates (Monte Carlo cross validation) the models that we report in the paper 
and generates our evaluation graphs: Figures 9-13.
- *models-graphs.* Code used to generate Figures 2-5, which motivate and explain our models.
- *stopping-criteria-experiments-results.* The code in this folder implements our stopping criteria and the experiments that we report in Section 6.2. There is also code for generating Figures 14-16.
