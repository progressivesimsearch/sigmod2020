# Clean the memory from all objects
rm(list=ls())

#library(microbenchmark)

require("ks")
# Check: https://cran.r-project.org/web/packages/ks/ks.pdf

dataset <- "deep1b"

N <- 100 # Testing iterations
T <- 20 # Number of discrete moments

k <- 100 #Number of training queries
#k <- 50

#epsilon <- 0.01 # To vary to generate other results
epsilon <- 0.05

theta <- 0.05

csvfile <- paste("output/k-", k, "-epsilon-", epsilon, ".csv", sep="")

summary.table <- function(filename){
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    
    #print(data.bsf[1:30,])
    
    data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    # data.bsf$time <- log2(data.bsf$time)
    #data.bsf$totalTime <- log2(data.bsf$totalTime)
    
    #    data.bsf <- data.bsf[data.bsf$leavesVisited > 0,]
    
    #data.bsf$leavesVisited <- log2(data.bsf$leavesVisited)
    #data.bsf$totalLeavesVisited <- log2(data.bsf$totalLeavesVisited)
    
    data.bsf$leavesVisited <- data.bsf$time
    
    data.bsf$totalLeavesVisited <- data.bsf$totalTime
    
    data.bsf$distance <- sqrt(data.bsf$distance )
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    best$answerId <- -1
    best <- aggregate(.~queryId, data = best, FUN = min)
    merge(data.bsf, best, by="queryId")[,c(1,2,4,5,6,7,3,9)]
 }


pi <- function(leaves, dist, kde, level = 0.95) {
    if(is.na(kde)) {
        return(c(dist, dist, dist))
    }
    
    x <- unlist(kde$eval.points[1])
    y <- unlist(kde$eval.points[2])
    z <- kde$estimate
    
    # find the index with the closest distance to the given one
    # xindex <- which(abs(x-dist)==min(abs(x-dist)))
    #densities <- z[xindex ,]/sum(z[xindex,])
    
    #Better solution: perform an interpolation rather than choose the closest value
    # I repeat this for all ys
    densities <- unlist(lapply(1:length(y), function(i){approx(x, z[,i], dist)$y}))
    densities <- densities / sum(densities)
    cumulative <- cumsum(densities)
    
    alpha <- 1 - level
    index1 <- tail(which(cumulative < alpha/2), 1)
    # Check this, the alpha level is quite conservative given the fact
    # that dist is an upper bound
    index2 <- which(cumulative > 1 - alpha/2)[1]
    
    integral <- round(sum(sapply(1:length(densities), function(x) x*densities[x])))
    
    # Final 1NN distance cannot be larger than approximate distance
    #ci <- c(min(dist, y[integral]), max(0, y[index1]), min(dist, y[index2]))
    
    ci <- c(min(dist, y[integral]), max(0, y[index1]), dist)
    
    ci
}

pi.error <- function(dist, pi.distance){
    dist/pi.distance - 1
}

results <- data.frame(k = integer(), dataset = character(), i = integer(), num = integer(), qid = integer(), typeI = integer(), typeIb = integer(), eq = double(), foundtime = integer(), time = integer(), timeTotal = integer(), percent = double(), distance = double(), stringsAsFactors=FALSE)

# Data preparation #################################################
queries.file <- paste("data/", dataset, ".csv", sep="")

# Data preparation #################################################
queries <- summary.table(queries.file)
queries <- queries[queries$distance.x < 10000,]
queries <- queries[queries$distance.y > 0,]

# I divide the max possible time into T quanta
# Consider the multiplier here!!!!!
max <- 1.2*max(queries$leavesVisited)
quanta <- seq(from = 0, to = max, by = max/T)
ids <- unique(queries$queryId)

for(i in 1:N){
    models <- list()
    
    training.ids <- sample(ids, k)
    testing.ids <- ids[-training.ids]
    # This is for testing...
    # testing.ids <- sample(queries$queryId[-training.ids], 10)

    testing <- queries[queries$queryId %in% testing.ids,]
    training <- queries[queries$queryId %in% training.ids,]
    
    trainingData = data.frame(quantum = double(), visited = double(), distance = double(), distance.1nn = double())
    counter <- 0
    
    # We need to train the model for the full tie slots till the end of the search.
    for(qid in training.ids){
        query <- training[training$queryId == qid,]
        j <- 1
        counter <- counter + 1
        
        results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, -1, 0, 0, query[j,]$time.x, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
        
        for(v in 0:T){
            t <- quanta[v+1]
            if(is.na(query[j,]$totalLeavesVisited.x)){
                break
            }
            else if(query[j,]$totalLeavesVisited.x < t) {
                break
                # trainingData[nrow(trainingData) + 1,] <- list(t, query[j,]$leavesVisited.x, query[j,]$distance.y, query[j,]$distance.y)
            } else {
                if(query[j,]$leavesVisited.x < t){
                    while(j < nrow(query) && query[j+1,]$leavesVisited.x < t) j <- j + 1
                }
                
                trainingData[nrow(trainingData) + 1,] <- list(t, query[j,]$leavesVisited.x, query[j,]$distance.x, query[j,]$distance.y)
            }
        }
    }
    #mbm <- microbenchmark(
    for(v in 0:T){
        t <- quanta[v+1]
        data <- trainingData[trainingData$quantum %in% t,]
        if(nrow(data) < 3) models[[v+1]] <- NA
        else tryCatch ({
            matr <- matrix(nrow = length(data$distance), ncol=2)
            matr[,1] <- data$distance + abs(rnorm(length(data$distance), 0, .000001))
            matr[,2] <- data$distance.1nn
            
            #density <- kde(x=matr)
            # h<-Hscv(matr) # It's important to have a good method for selecting the bandwidth!!!
            # density <- kde(x=matr, H = h, gridsize = 200)
            density <- kde(x=matr, gridsize = 200)
            
            models[[v+1]] <- density
        }, error = function(err) {
            models[[v+1]] <- NA
        })
    }#, times = 1)
    #print(mbm)
    ##########################################################
    
    for(qid in testing.ids){
        query <- testing[testing$queryId == qid,]
        j <- 1
        stopped <- FALSE
        counter <- counter + 1
        
        for(v in 0:T){
            t <- quanta[v+1]
            
            # Estimate the current relative error
            if(query[j,]$totalLeavesVisited.x < t){
              stopped <- TRUE
              results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, 0, 0, 0, query[j,]$time.x, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
              break;
            }

            if(query[j,]$leavesVisited.x < t){
                while(j < nrow(query) && query[j+1,]$leavesVisited.x < t) j <- j + 1
            }

            # Get the 95% CI for the distance
            PI <- pi(t, query[j,]$distance.x, models[[v+1]], level = (1 - 2*theta))
            
            # And now, find the 95% CI of the relative error
            PI.error <- pi.error(query[j,]$distance.x, PI)

            # Also find the real elative error
            eq <- query[j,]$distance.x / query[j,]$distance.y - 1
            
            if(PI.error[2] < epsilon) { # This is the stopping condition
                #cat(dataset, i, v, "\n")
                
                if(eq < PI.error[3] | eq > PI.error[2]) typeI <- 1
                else typeI <- 0
                
                if(eq > epsilon) typeIb <- 1
                else typeIb <- 0
                
                # cat(i, qid, v, typeI, eq, (query[j,]$time.x / query[j,]$totalTime.x), "\n")
                stopped <- TRUE
                time = t
                results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, typeI, typeIb, eq, query[j,]$time.x, time, query[j,]$totalTime.x, (time / query[j,]$totalTime.x), query[j,]$distance.y)
                
                break
            }
        }
        if(!stopped){
            results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, 0, 0, 0, query[j,]$time.x, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
        }
    }
}


write.csv(results, file=csvfile)
