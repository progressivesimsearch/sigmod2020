# Created by Theophanis Tsandilas, Inria
# Stopping based on Logistic regression to assess the probability that the exact answer has been found

# Clean the memory from all objects
rm(list=ls())

# General process.
# 1000 or 5000 series are used as the pool of training and testing queries (T = R + S)
# Size of S (testing queries per iteration) N.test = 200 (constant)
# Size of R (training queries) k = 50, 100, 200, 500
# We follow a Monte Carlo validation approach that repeats  N = 100 times the following steps:
# 1. We randomly draw a sample (from T) for the testing set S
# 2. From the remaining series, we randomly draw a sample of size k for the training set R
# 3. We train the model and then test it
# 4. We repeat

dataset <- "deep1b"

N <- 20 # Testing iterations
T <- 20 # Number of discrete moments

k <- 50 #Number of training queries
#k <- 50

phi <- 0.01 # To vary for other phi values
# phi <- 0.05
# phi <- 0.10

csvfile <- paste("output/k-", k, "-probability-phi-", phi, ".csv", sep="")

summary.table <- function(filename){
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    
    data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    #data.bsf <- data.bsf[data.bsf$leavesVisited > 0,]

    #data.bsf$time <- data.bsf$time/1000000
    #data.bsf$totalTime <- data.bsf$totalTime/1000000
    
    # Attention! Remove this if you really want prediction to be performed on the number of visited leaves
    # I have this just for practical reasons to make comparions based on time rather than number of leaves
    data.bsf$leavesVisited <- data.bsf$time/1000000
    data.bsf$totalLeavesVisited <- data.bsf$totalTime/1000000
    
    data.bsf$time <- data.bsf$time/1000000
    data.bsf$totalTime <- data.bsf$totalTime/1000000
    
    data.bsf$distance <- sqrt(data.bsf$distance )
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    best$answerId <- -1
    best <- aggregate(.~queryId, data = best, FUN = min)
    merge(data.bsf, best, by="queryId")[,c(1,2,4,5,6,7,3,9)]
}


pi <- function(leaves, dist, lmodel, level = 0.95) {
    newdata = data.frame(distance = dist, quantum = leaves)
    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
    if(is.nan(pi[2])) pi[2] <- dist
    if(is.nan(pi[3])) pi[3] <- dist
    
    c(min(dist, pi[1]), max(0, pi[2]), min(dist, pi[3]))
}

lpredict <- function(quantum, dist, time, logitMod){
     testData <- data.frame(distance = dist, diff = max(0, time - quantum))
     #plogis(predict(logitMod, testData))
     predict(logitMod, testData, type="response", se.fit=TRUE)
}


pi.error <- function(dist, pi.distance){
    dist/pi.distance - 1
}


results <- data.frame(k = integer(), dataset = character(), i = integer(), num = integer(), qid = integer(), typeI = integer(), typeIb = integer(), eq = double(), time = integer(), timeTotal = integer(), percent = double(), distance = double(), stringsAsFactors=FALSE)

# Data preparation #################################################
queries.file <- paste("data/", dataset, ".csv", sep="")

# Data preparation #################################################
queries <- summary.table(queries.file)

queries <- queries[queries$distance.x < 10000,]
queries <- queries[queries$distance.y > 0,]

# I divide the max possible time into T quanta
max <- 1.2*max(queries$leavesVisited)
quanta <- seq(from = 0, to = max, by = max/T)

ids <- unique(queries$queryId)
for(i in 1:N){
    models <- list()
    
    training.ids <- sample(ids, k)
    testing.ids <- ids[-training.ids]
    # This is for testing...
    #testing.ids <- sample(queries$queryId[-training.ids], 50)
    
    testing <- queries[queries$queryId %in% testing.ids,]
    training <- queries[queries$queryId %in% training.ids,]
    
    trainingData = data.frame(quantum = double(), visited = double(), distance = double(), distance.1nn = double(), diff = double(), final = logical(), total = double())
    
    counter <- 0
    
    # We need to train the model for the full tie slots till the end of the search.
    for(qid in training.ids){
        
        query <- training[training$queryId == qid,]
        j <- 1
        counter <- counter + 1
        
        results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, -1, 0, 0, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
        
        for(v in 0:T){
            t <- quanta[v+1]
            if(is.na(query[j,]$totalLeavesVisited.x)) break
            else if(query[j,]$totalLeavesVisited.x < t){
                break;
                #trainingData[nrow(trainingData) + 1,] <- list(t, query[j,]$totalLeavesVisited.x, query[j,]$distance.y, query[j,]$distance.y, max(0, t - query[j,]$leavesVisited.x), query[j,]$distance.x == query[j,]$distance.y, query[j,]$totalLeavesVisited.x)
            }
            else {
                if(query[j,]$leavesVisited.x < t){
                    while(j < nrow(query) && query[j+1,]$leavesVisited.x < t) j <- j + 1
                }
            
                trainingData[nrow(trainingData) + 1,] <- list(t, query[j,]$leavesVisited.x, query[j,]$distance.x, query[j,]$distance.y, max(0, t - query[j,]$leavesVisited.x), query[j,]$distance.x == query[j,]$distance.y, query[j,]$totalLeavesVisited.x)
            }
        }
    }
    
    for(v in 0:T){
        t <- quanta[v+1]
        data <- trainingData[trainingData$quantum %in% t,]
        
        if(nrow(data) > 1) {
            model <- glm(final ~ distance, data=data, family=binomial(link="logit"))
            models[[v+1]] <- model
            
            # cat(dataset, v, "\n")
            #print(summary(model))
            
        } else models[[v+1]] <- models[[v]]
    }
    ##########################################################
    
    for(qid in testing.ids){
        query <- testing[testing$queryId == qid,]
        j <- 1
        stopped <- FALSE
        counter <- counter + 1
        
        for(v in 0:T){
            t <- quanta[v+1]
            
            # Estimate the current relative error
            if(query[j,]$totalLeavesVisited.x < t){
              stopped <- TRUE
              results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, 0, 0, 0, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
              v <- T + 1
              break
            }

            if(query[j,]$leavesVisited.x < t){
                while(j < nrow(query) && query[j+1,]$leavesVisited.x < t) j <- j + 1
            }

            logitMod <- models[[v+1]]
            predicted <- lpredict(t, query[j,]$distance.x, query[j,]$time.x, models[[v+1]])
            
            
            #lower <- predicted$fit - (1.96*predicted$se.fit) # lower bounds
            #upper <- predicted$fit + (1.96*predicted$se.fit) # upper bounds
            # cat(v, t, predicted$fit, "\n")
            
            prob <- predicted$fit#summary(predicted)[4]
            if(prob > (1 - phi)) { # This is the stopping condition
                if(query[j,]$distance.y < query[j,]$distance.x){
                    typeIb <- 1
                    typeI <- 1
                }
                else{
                    typeI <- 0
                    typeIb <- 0
                }
                
                stopped <- TRUE
                #cat(i, qid, v, typeI, eq, (query[j,]$time.x / query[j,]$totalTime.x), "\n")
                
                time = t
                results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, typeI, typeIb, 0, time, query[j,]$totalTime.x, (time / query[j,]$totalTime.x), query[j,]$distance.y)
                
                v <- T + 1
                break
            }
        }
        
        # May never happen, but it happens in some cases...
        if(!stopped){
              results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, 0, 0, 0, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
        }
    }
}

write.csv(results, file=csvfile)
