# Created by Theophanis Tsandilas, Inria

# Clean the memory from all objects
rm(list=ls())

library(gridExtra)
library(grid)
library("ggplot2")
library("ggpubr")
library("tidyr")
library(scales)

filename1 <- "results/k-100-epsilon-0.01.csv"
filename2 <- "results/k-100-epsilon-0.05.csv"
filename3 <- "results/k-50-epsilon-0.01.csv"
filename4 <- "results/k-50-epsilon-0.05.csv"

getResults <- function(filename, k, dataset = "deep1b"){
    data <- read.csv(file=filename, header=TRUE, sep=",")
    data <- data[data$dataset == dataset,]
    data <- data[data$k == k,]
    
    data <- data[c("num", "eq", "time", "timeTotal", "typeIb")]
    data$exact <- (data$eq == 0)
    data <- aggregate(.~num, data = data, FUN = mean)[c("num", "time", "timeTotal", "exact", "typeIb")]
    
    data$cumTime <- cumsum(data$time)
    data$cumTimeTotal <- cumsum(data$timeTotal)
    
    data[c("num", "cumTime", "cumTimeTotal", "exact", "typeIb")]
}

epsilon.01 <- getResults(filename1, k = 100)
epsilon.01$criterion <- "epsilon = .01 (k = 100)"
epsilon.01$epsilon <- .01
epsilon.01$k <- 100
dat.1 <- epsilon.01[epsilon.01$num > 100,]
cat(mean(dat.1$typeIb), mean(dat.1$exact), "\n")

epsilon.05 <- getResults(filename2, k = 100)
epsilon.05$criterion <- "epsilon = .05 (k = 100)"
epsilon.05$epsilon <- .05
epsilon.05$k <- 100
dat.2 <- epsilon.05[epsilon.05$num > 100,]
cat(mean(dat.2$typeIb), mean(dat.2$exact), "\n")

epsilon50.01 <- getResults(filename3, k=50)
epsilon50.01$criterion <- "epsilon = .01 (k = 50)"
epsilon50.01$epsilon <- .01
epsilon50.01$k <- 50
dat.3 <- epsilon50.01[epsilon50.01$num > 50,]
cat(mean(dat.3$typeIb), mean(dat.3$exact), "\n")

epsilon50.05 <- getResults(filename4, k=50)
epsilon50.05$criterion <- "epsilon = .05 (k = 50)"
epsilon50.05$epsilon <- .05
epsilon50.05$k <- 50
dat.4 <- epsilon50.05[epsilon50.05$num > 50,]
cat(mean(dat.4$typeIb), mean(dat.4$exact), "\n")

default <- data.frame("num" = epsilon.01$num, "cumTime" = (epsilon.01$cumTimeTotal), cumTimeTotal = epsilon.01$cumTimeTotal, "typeIb" = 0, "exact" = 1, "k" = 0, "epsilon" = 0)
default$criterion <- "default"

data.coverage <- rbind(dat.1, dat.2, dat.3, dat.4)

data <- rbind(epsilon.01, epsilon.05, epsilon50.01, epsilon50.05, default)
data$cumTime <- data$cumTime/(1000000*3600)

cbPalette1 <- c("#555555", "#E69F00", "#009E73", "#E69F00", "#009E73")
cbPalette2 <- c("#009E73", "#E69F00")
shapes <- c(1, 2, 3, 8)

plot1 <- ggplot(data = data, aes(x=num, y=cumTime, group=criterion)) +
    geom_line(size = 0.6, aes(color=factor(criterion))) +
    theme_bw() +
    theme( legend.position = "none", legend.title = element_blank(),
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("Queries") +
    ylim(0, 80) +
    ylab("Time (hours)") +
    scale_colour_manual(values=cbPalette1)

data2 <- data.coverage[grepl("epsilon", data.coverage$criterion),]
data2 <- aggregate(exact~epsilon+k, data = data2, FUN = mean)

plot2 <- ggplot(data = data2, aes(x=factor(epsilon), y=100*exact, group=factor(k))) +
    geom_point(size = c(2,2,3,3), aes(color=factor(k), shape=factor(epsilon))) +
    geom_line(aes(color=factor(k))) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none",
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("epsilon") +
    ylab("Exact Answers (%)") +
    ylim(93, 100) +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c(".01", ".05"), expand = c(0.3, 0.3)) +
    scale_colour_manual(values=cbPalette2)

data2 <- data.coverage[grepl("epsilon", data.coverage$criterion),]
data2 <- aggregate(typeIb~epsilon+k, data = data.coverage, FUN = mean)

plot3 <- ggplot(data = data2, aes(x=factor(epsilon), y=100*(1-typeIb), group=factor(k))) +
    geom_point(size = c(2,2,3,3), aes(color=factor(k), shape=factor(epsilon))) +
    geom_line(aes(color=factor(k))) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none",
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("epsilon") +
    ylab("eQ < epsilon (%)") +
    ylim(94, 100) +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c(".01", ".05"), expand = c(0.3, 0.3)) +
    scale_colour_manual(values=cbPalette2)

grid.arrange(plot3, plot2, plot1, nrow = 1, widths=c(1, 1, 1.7))
