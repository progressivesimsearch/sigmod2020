# Clean the memory from all objects
rm(list=ls())

library("ggplot2")
library("ggpubr")
library(scales)

library(gridExtra)
library(grid)

filename1 <- "results/isax-time-stopping-0.01.csv"
filename2 <- "results/isax-time-stopping-0.05.csv"
filename3 <- "results/isax-time-stopping-0.1.csv"

filename4 <- "results/dstree-time-stopping-0.01.csv"
filename5 <- "results/dstree-time-stopping-0.05.csv"
filename6 <- "results/dstree-time-stopping-0.1.csv"

getResults <- function(prefix, filename){
    data <- read.csv(file=filename, header=TRUE, sep=",")
    
    data$exact <- (data$eq == 0)
    data$eq05 <- (data$eq > 0.05)
    data$eq01 <- (data$eq > 0.01)
    
    results <- aggregate(.~k+dataset, data = data, FUN = mean)[c(1,2,6,7, 9, 10, 11, 12)]
    
    names(results)[5] <- "timePercent"
    results$index <- paste(prefix, sub(".*/", "", sub("-.*", "", filename)), sep ="")
    
    results
}

isax.01 <- getResults("1.", filename1)
isax.05 <- getResults("1.", filename2)
isax.10 <- getResults("1.", filename3)

dstree.01 <- getResults("2.", filename4)
dstree.05 <- getResults("2.", filename5)
dstree.10 <- getResults("2.", filename6)

data.01 <- rbind(dstree.01, isax.01)
#data.01 <- aggregate(data = data.01, .~dataset, FUN = mean)
data.01$phi <- 0.01

data.05 <- rbind(dstree.05, isax.05)
#data.05 <- aggregate(data = data.05, .~dataset, FUN = mean)
data.05$phi <- 0.05

data.10 <- rbind(dstree.10, isax.10)
#data.10 <- aggregate(data = data.10, .~dataset, FUN = mean)
data.10$phi <- 0.1

data <- rbind(data.01, data.05, data.10)

##################################################################
##################################################################
##################################################################

cbPalette <- c( "#0072B2",  "#D55E00", "#E69F00","#009E73")
shapes <- c(1, 2, 3, 8)

plot1 <- ggplot(data = data, aes(x=factor(phi), y=100*exact, group=dataset)) +
    geom_point(size = 2, aes(color=dataset, shape=dataset)) +
    geom_line(aes(color=dataset)) +
    facet_wrap(~index, ncol=2) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none", legend.title = element_blank(),
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("phi") +
    ylab("Exact Answers (%)") +
    ylim(85, 100) +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c(".01", ".05", ".10")) +
    scale_colour_manual(values=cbPalette)

plot2 <- ggplot(data = data, aes(x=factor(phi), y=(1-timePercent), group=dataset)) +
    geom_point(size = 2, aes(color=dataset, shape=dataset)) +
    geom_line(aes(color=dataset)) +
    facet_wrap(~index, ncol=2) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none", legend.title = element_blank(),
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("phi") +
    ylab("Time Savings (%)") +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c(".01", ".05", ".10")) +
    scale_y_continuous(breaks = c(0.4,.6, .8, 1), labels = c("40", "60", "80", "100"), limits = c(0.3, 1)) +
    scale_colour_manual(values=cbPalette)
