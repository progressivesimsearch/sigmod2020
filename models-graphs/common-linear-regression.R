# Plots to evaluate the common linear regression model
# Not included in SIGMOD submission

# Clean the memory from all objects
rm(list=ls())

datasets <- c("seismic", "sald", "deep1b", "synthetic")

# Change to get results for dstree
algorithm <- "isax"

N.test <- 500
k <- 100 # Number of training queries

confidence = 0.95 # I set a 95% confidence level for the prediction intervals

summary.table <- function(filename){
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    #data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    data.bsf <- data.bsf[,c(2,3,4,5,6,7,8)]
    data.bsf$time <- log(data.bsf$time)
    data.bsf$totalTime <- log(data.bsf$totalTime)
    data.bsf$seriesCompared <- log(data.bsf$seriesCompared)
    data.bsf$totalSeriesCompared <- log(data.bsf$totalSeriesCompared)
    data.bsf$distance <- sqrt(data.bsf$distance )
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    
    merge(data.bsf, best, by="queryId")[,c(1,4,5,6,7,3,9)]
}

pi <- function(leaves, dist, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = dist, seriesCompared.x = leaves)
    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
    c(pi[1], max(0, pi[2]), min(dist, pi[3]))
}


models = list()
par(mfrow=c(1,4), mar = c(2,2,2,1))

prefix <- paste("../datasets/", algorithm, "/", sep="")

for(dataset in datasets){
    # Data preparation #################################################
    queries.file <- paste(prefix, dataset, ".csv", sep="")
    
    # Data preparation #################################################
    queries <- summary.table(queries.file)
    queries <- queries[queries$distance.x < 10000,]
    # queries <- queries[queries$leavesVisited.x > 0,]
    #  queries$distance.x <- sqrt(queries$distance.x)
    # queries$distance.y <- sqrt(queries$distance.y)
    #queries <- queries[, c(1, 3, 7)]
    
    # colnames(queries)[1] <- "queryID"

    testing.ids <- sample(nrow(queries), N.test)
    testing <- queries[testing.ids,]
    training <- queries[-testing.ids,]
    training <- training[sample(nrow(training), k),]
    
    lmodel <- lm(distance.y ~ distance.x + seriesCompared.x + distance.x*seriesCompared.x, data = training)
    
    # lmodel <- lm(distance.y ~ distance.x + time.x, data = training)
    models[[dataset]] <- lmodel
    
    training <- testing
    
    if(dataset == "seismic"){
        lim = c(0,20)
    } else if(dataset == "sald") {
        lim = c(0,13)
    } else if(dataset == "deep1b") {
         lim = c(0,8)
    }
    else {
        lim = c(0,12)
    }
    
    plot(y=training$distance.y, x= summary(lmodel)$coef[1] +summary(lmodel)$coef[2]*training$distance.x +summary(lmodel)$coef[3]*training$seriesCompared.x + summary(lmodel)$coef[4]*training$distance.x*training$seriesCompared.x, pch = 19, col = adjustcolor("orange", alpha.f = 0.2), xaxs="i", yaxs="i", xlab = NA, ylab = NA, bty="n", xlim = lim, ylim = lim)
    par(new=TRUE)
    curve((x), xlab = NA, ylab = NA, bty="n", xlim = lim, ylim = lim, xaxs="i", yaxs="i", col="red", axes = FALSE, lwd=1)
}
