# Author: Theophanis Tsandilas, Inria
# Graphs the witness-based models for initial query-sensitive estimates

# Clean the memory from all objects
rm(list=ls())

require("ks")
# Check: https://cran.r-project.org/web/packages/ks/ks.pdf

datasets <- c("seismic", "sald", "deep1b", "synthetic")

N.test <- 500
k <- 100 # Number of training queries
W <- 200 # Number of witnesses
N <- 1 # Testing iterations

confidence = 0.95 # I set a 95% confidence level for the prediction intervals

summary.table <- function(filename=paste(dataset, "/test/log_1nn_", test.size, "q_file", test.n, ".csv", sep="")){
    data <- read.csv(file=filename, header=TRUE, sep=",")

    data <- data[,c(2,3,4,5,6)]
    data$time <- data$time/ 1000000
    data$totalTime <- data$totalTime/ 1000000

    data
}

aggregate.wit <- function(df){
	df$dist <- (1/df$dist)^5 # This seems to work well!!!

	df.1 <- aggregate(dist~queryID, df, FUN = sum)
	df$product <- df$dist*df$distance
	df <- aggregate(product~queryID, df, FUN = sum)

	df$distance <- df$product/df.1$dist

	df[-c(2)]
}

pi <- function(wit, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = wit)
    predict(lmodel, newdata, interval="predict", level = level)
}

models = list()
par(mfrow=c(1,4), mar = c(2,2,2,1))

prefix <- paste("../datasets/witnesses/",sep="")

for(dataset in datasets){
    # Data preparation #################################################
    wit.file <- paste(prefix, dataset, "-witnesses.csv", sep="")
    queries.file <- paste(prefix, dataset, "-queries.csv", sep="")
    dist.file <- paste(prefix, dataset, "-dist.csv", sep="")

    # Data preparation #################################################
    queries <- summary.table(queries.file)
    queries$distance <- sqrt(queries$distance)
    queries <- queries[, c(1, 3)]

    witnesses <- read.csv(file=wit.file, header=TRUE, sep=",")
    witnesses <- witnesses[,c(2,4)]
    witnesses$distance <- sqrt(witnesses$distance)

    quer.wit <- read.csv(file=dist.file, header=TRUE, sep=",")

    colnames(queries)[1] <- "queryID"
    colnames(witnesses)[1] <- "witnessID"
    colnames(quer.wit)[3] <- "dist"

    data <- merge(quer.wit, witnesses, by = "witnessID")
    
    witness.ids <- sample(unique(data$witnessID), W)
    data.filtered <- data[data$witnessID %in% witness.ids,]
    df <- aggregate.wit(data.filtered)
    
    testing.ids <- sample(nrow(queries), N.test)
    testing <- queries[testing.ids,]
    training <- queries[-testing.ids,]
    training <- training[sample(nrow(training), k),]
    
    df.0 <- merge(df, training, by="queryID")
    lmodel <- lm(distance.y ~ distance.x, data = df.0)
    models[[dataset]] <- lmodel

    df.1 <- merge(df, testing, by="queryID")
    
    newx <- seq(0, 13, by=0.05)

    if(dataset == "seismic"){
        plot(df.1$distance.x, df.1$distance.y, pch = 19, col = adjustcolor("orange", alpha.f = 0.2), xlab = NA, ylab = NA, bty="n", xlim = c(0,12.5), ylim = c(0, 17.5), xaxs="i", yaxs="i")
    }
    else if(dataset == "synthetic") {
        plot(df.1$distance.x, df.1$distance.y, pch = 19, col = adjustcolor("orange", alpha.f = 0.2), xlab = NA, ylab = NA, bty="n", xlim = c(0,8), ylim = c(0, 12), xaxs="i", yaxs="i")
    }
    else if(dataset == "sald") {
                plot(df.1$distance.x, df.1$distance.y, pch = 19, col = adjustcolor("orange", alpha.f = 0.2), xlab = NA, ylab = NA, bty="n", xlim = c(0,5.5), ylim = c(0, 12), xaxs="i", yaxs="i")
    } else {
                plot(df.1$distance.x, df.1$distance.y, pch = 19, col = adjustcolor("orange", alpha.f = 0.2), xlab = NA, ylab = NA, bty="n", xlim = c(0,5.5), ylim = c(0, 8), xaxs="i", yaxs="i")
    }
    
    
    abline(lmodel, col = "red", lwd = 2)
    pred.int =  predict(lmodel, newdata=data.frame(distance.x=newx), interval="prediction")
    pred.lower = pred.int[,2]
    pred.upper = pred.int[,3]

    lines(newx, pred.lower, lwd=2.5, col="blue", lty=2)
    lines(newx, pred.upper, lwd=2.5, col="blue", lty=2)
}
