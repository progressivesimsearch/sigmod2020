# Clean the memory from all objects
rm(list=ls())

# Package for quantile regression
require("quantreg")

# General process.
# 5000 or 1000 series are used as the pool of training and testing queries (T = R + S)
# Size of S (testing queries per iteration) N.test = 200 (constant)
# Size of R (training queries) k = 50, 100, 200, 500
# We follow a Monte Carlo validation approach that repeats  N = 100 times the following steps:
# 1. We randomly draw a sample (from T) for the testing set S
# 2. From the remaining series, we randomly draw a sample of size k for the training set R
# 3. We train the model and then test it
# 4. We repeat

datasets <- c("synthetic", "seismic", "sald", "deep1b")
algorithms <- c("DSTree", "iSAX")

N.test <- 400
ks <- c(25, 50, 100, 200) # Number of training queries
N <- 200 # Testing iterations

csvfile <- "output/exact-coverage.csv"


summary.table <- function(filename){
    logId <- 0 # When to provide estimates (Here, leaf = 1)
    
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    #data.bsf$time <- data.bsf$time/ 1000000
    #data.bsf$totalTime <- data.bsf$totalTime/ 1000000
    
    data.bsf$leavesVisited <- log2(data.bsf$leavesVisited)
    
    selected <- data.bsf[data.bsf$leavesVisited <= logId,]
    
    #    data.bsf$leavesVisited <- log2(data.bsf$time)
    
    appr <- aggregate(distance ~ queryId, data = selected, min)
    best <- data.bsf[data.bsf$answerId=="bestNN",]

    merge(appr, best, by="queryId")
}

#pi <- function(dist, lmodel, level = 0.9) {
#    newdata = data.frame(distance.x = dist)
#    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
#    c(pi[1], max(0, pi[2]), min(dist, pi[3]))
#}

getCoverage <- function(intercept, slope, testing){
    predict = testing$distance.x * slope + intercept
    
    correct <- (testing$leavesVisited <= predict)
    
    sum(correct) / length(correct)
}


results <- data.frame(algorithm = character(), k = integer(), dataset = character(), coverage.95 = double(), coverage.99 = double(), stringsAsFactors=FALSE)


for(algorithm in algorithms){
    prefix <- paste("../../datasets/", algorithm, "/", sep="")
    
    for(k in ks){
        for(dataset in datasets){
            # Data preparation #################################################
            queries.file <- paste(prefix, dataset, ".csv", sep="")
            
            # Data preparation #################################################
            queries <- summary.table(queries.file)
            queries <- queries[queries$distance.x < 10000,]
            queries$distance.x <- sqrt(queries$distance.x)
            queries$distance.y <- sqrt(queries$distance.y)
            
            colnames(queries)[1] <- "queryID"

            coverage.95 <- 0
            coverage.99 <- 0
            for(i in 1:N){
                testing.ids <- sample(nrow(queries), N.test)
                testing <- queries[testing.ids,]
                training <- queries[-testing.ids,]
                training <- training[sample(nrow(training), k),]
                
                model.95 <- rq(leavesVisited ~ distance.x, data = training, tau = 0.95)
                model.99 <- rq(leavesVisited ~ distance.x, data = training, tau = 0.99)
                
                coverage.95 <- coverage.95 + getCoverage(model.95$coefficients[[1]], model.95$coefficients[[2]], testing)
                coverage.99 <- coverage.99 + getCoverage(model.99$coefficients[[1]], model.99$coefficients[[2]], testing)
            }
            
            results[nrow(results) + 1, ] <- list(algorithm, k, dataset, coverage.95/N, coverage.99/N)
        }
    }
}

#models0 <- models
write.csv(results, file=csvfile)
