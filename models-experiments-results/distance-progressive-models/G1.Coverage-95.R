# Created by Theophanis Tsandilas

# Clean the memory from all objects
rm(list=ls())

library(gridExtra)
library(grid)

library("ggplot2")
library("ggpubr")
library(scales)

algorithm <- "dstree"
models <- c("indiv-linear", "2D-kernel", "3D-kernel")

getPlot <- function(algorithm, model, ylab = FALSE){
    timepoints <- c(0, 2, 4, 6, 8, 10)
    
    filename <- paste("results/", algorithm, "-", model, ".csv", sep="")
    data2 <- read.csv(file=filename, header=TRUE, sep=",")
    
    data2 <- data2[data2$Nr < 300,]
    data2 <- data2[data2$time %in% timepoints,]
    
    cov.95 <- aggregate(coverage.95~Nr+time, data2, FUN=mean)
    colnames(cov.95)[3] <- "coverage"
    cov.99 <- aggregate(coverage.99~Nr+time, data2, FUN=mean)
    colnames(cov.99)[3] <- "coverage"
    
    cbPalette <- c("#E69F00", "#D55E00", "#009E73", "#0072B2", "#CC79A7")
    
    data2 <- cov.95
    data2$coverage <- data2$coverage*100
    data2$time <- 2^data2$time
    
    plot <- ggplot(data = data2, aes(x=factor(data2$time), y=data2$coverage)) +
    geom_point(size = 2, aes(shape=factor(data2$Nr), colour=factor(data2$Nr))) +
    geom_line(aes(colour=factor(data2$Nr), group=data2$Nr)) +
    scale_shape_manual(values = c(0, 2, 8, 15, 19)) +
    theme_bw() +
    labs(fill = "Training Size") +
    {if(ylab == FALSE) theme(axis.title.y=element_blank(), legend.position = "none",  axis.text.y = element_blank(), axis.ticks.y = element_blank(),plot.margin = unit(c(.2, .2, .15, .1), "cm"))
        else theme(legend.position = "none",
        plot.margin = unit(c(.2, .2, .15, .1), "cm"))
    } +
    xlab("Leaf Visits (log2)") +
    ylab("Coverage (%)") +
    ylim(90, 100) +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) + scale_colour_manual(values=cbPalette) + scale_x_discrete(labels=timepoints, expand = c(0.04, 0.04)) +
    labs(title = model)+
    theme(plot.title = element_text(hjust = 0.5, size = 11))
    
    plot
}

plot1 <- getPlot(algorithm, models[1], TRUE)
plot2 <- getPlot(algorithm, models[2])
plot3 <- getPlot(algorithm, models[3])

grid.arrange(plot1, plot2, plot3, nrow = 1, widths=c(1.3,1,1))

