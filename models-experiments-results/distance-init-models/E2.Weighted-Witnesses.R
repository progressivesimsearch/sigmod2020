# Created by Theophanis Tsandilas, Inria

# Clean the memory from all objects
rm(list=ls())

# General process.
# 1000 series are used as the pool of witnesses (W)
# 1000 series are used as the pool of training and testing queries (T = R + S)
# Number of witnesses W = 50, 100, 200, 500
# Size of S (testing queries per iteration) N.test = 200 (constant)
# Size of R (training queries) k = 50, 100, 200, 500
# We follow a Monte Carlo validation approach that repeats  N = 100 times the following steps:
# 1. We randomly draw a sample (from T) for the testing set S
# 2. From the remaining series, we randomly draw a sample of size k for the training set R
# 3. We train the model and then test it
# 4. We repeat

datasets <- c("seismic", "sald", "deep1b", "synthetic")

csvfile <- "output/witnesses-linear.csv"

N.test <- 200
N <- 100 # Testing iterations

ks <- c(25, 50, 100, 200) # Number of training queries
Ws <- c(50, 100, 200, 500) # Number of witnesses

prefix <- paste("../../datasets/witnesses/", sep="")


summary.table <- function(filename){
    data <- read.csv(file=filename, header=TRUE, sep=",")

    data <- data[,c(2,3,4,5,6)]
    data$time <- data$time/ 1000000
    data$totalTime <- data$totalTime/ 1000000

    data
}

aggregate.wit <- function(df){
	df$dist <- (1/df$dist)^5 # This seems to work well!!!

	df.1 <- aggregate(dist~queryID, df, FUN = sum)
	df$product <- df$dist*df$distance
	df <- aggregate(product~queryID, df, FUN = sum)

	df$distance <- df$product/df.1$dist

	df[-c(2)]
}

pi <- function(wit, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = wit)
    predict(lmodel, newdata, interval="predict", level = level)
}


results <- data.frame(dataset = character(), K = integer(), W = integer(), i= integer(), count = integer(),
    coverage.95 = double(), coverage.99 = double(), range.95 = double(), range.99 = double(), mse = double(), stringsAsFactors=FALSE)

for(k in ks){
    cat(k, "\n")
    for(W in Ws){
        cat(W, ": ")
        for(dataset in datasets){
            
            # Data preparation #################################################
            wit.file <- paste(prefix, dataset, "-witnesses.csv", sep="")
            queries.file <- paste(prefix, dataset, "-queries.csv", sep="")
            dist.file <- paste(prefix, dataset, "-dist.csv", sep="")
            
            # Data preparation #################################################
            queries <- summary.table(queries.file)
            queries$distance <- sqrt(queries$distance)
            queries <- queries[, c(1, 3)]
            
            witnesses <- read.csv(file=wit.file, header=TRUE, sep=",")
            witnesses <- witnesses[,c(2,4)]
            witnesses$distance <- sqrt(witnesses$distance)
            
            quer.wit <- read.csv(file=dist.file, header=TRUE, sep=",")
            
            colnames(queries)[1] <- "queryID"
            colnames(witnesses)[1] <- "witnessID"
            colnames(quer.wit)[3] <- "dist"
            
            data <- merge(quer.wit, witnesses, by = "witnessID")
            
            # Measures
            coverage.95 <- c()
            coverage.99 <- c()
            range.95 <- c()
            range.99 <- c()
            MSE <- c()
            
            cat(dataset, " ")
            
            for(i in 1:N){
                witness.ids <- sample(unique(data$witnessID), W)
                data.filtered <- data[data$witnessID %in% witness.ids,]
                df <- aggregate.wit(data.filtered)
                
                testing.ids <- sample(nrow(queries), N.test)
                testing <- queries[testing.ids,]
                training <- queries[-testing.ids,]
                training <- training[sample(nrow(training), k),]
                
                df.1 <- merge(df, training, by="queryID")
                lmodel <- lm(distance.y ~ distance.x, data = df.1)
                
                df.2 <- merge(df, testing, by="queryID")
                
                e.95 <- 0
                e.99 <- 0
                r.95 <- 0
                r.99 <- 0
                mse <- 0
                count <- 0
                for(j in 1:nrow(df.2)){
                    PI.95 <- pi(df.2$distance.x[j], lmodel, level = 0.95)
                    PI.99 <- pi(df.2$distance.x[j], lmodel, level = 0.99)
                    
                    if(!is.na(PI.95[3] - PI.95[2]) & !is.na(PI.99[3] - PI.99[2])){
                        count <- count + 1
                        
                        if(df.2$distance.y[j] < PI.95[2] || df.2$distance.y[j] > PI.95[3]) e.95 <- e.95 + 1
                        if(df.2$distance.y[j] < PI.99[2] || df.2$distance.y[j] > PI.99[3]) e.99 <- e.99 + 1
                        r.95 <- r.95 + (PI.95[3] - PI.95[2])
                        r.99 <- r.99 + (PI.99[3] - PI.99[2])
                        mse <- mse + (PI.95[1] - df.2$distance.y[j])^2
                    }
                }
                
                results[nrow(results) + 1, ] <- list(dataset, k, W, i, count, 1 - e.95/count, 1 - e.99/count,
                    r.95/count, r.99/count, mse/count)
            }
        }
        cat("\n")
    }
}

write.csv(results, file=csvfile)
